export const state = () => ({
    list: {}
})

export const mutations = {
    SAVE_LIB(state, data) {
        state.list = {
            ...state.list,
            [data.name]: data
        }
    }
}


export const actions = {
    fetchLib({commit}, data) {
        var tags, lib, usage, downloads;
        this.$axios.$get(`https://data.jsdelivr.com/v1/package/npm/${data.name}`).then((response) => {
            tags = response.tags
        })
        this.$axios.$get(`https://data.jsdelivr.com/v1/package/npm/${data.name}@${data.version}`).then((response) => {
            if (response.default !== '/lib/index') {
               lib = response.default

                this.$axios.$get(`https://data.jsdelivr.com/v1/package/npm/${data.name}@${data.version}/stats`).then((response) => {
                    let chart;

                    if (response['files'][lib]) {
                        chart = {
                            labels: [],
                            datasets: [{
                                label: 'Downloads',
                                data: []
                            }]
                        }
                        for (const [date, value] of Object.entries(response['files'][lib].dates)) {
                            chart.labels.push(date);
                            chart.datasets[0].data.push(value);
                        }
                    }

                    usage = chart;
                    downloads = response.total
                    commit('SAVE_LIB', {
                        name: data.name,
                        version: data.version,
                        author: data.author,
                        description: data.description,
                        tags: tags,
                        lib: lib,
                        usage: usage,
                        downloads: downloads
                    });
                })
            } else {
                commit('SAVE_LIB', {
                    name: data.name,
                    version: data.version,
                    author: data.author,
                    description: data.description,
                    tags: tags,
                    lib: lib,
                    usage: usage,
                    downloads: downloads
                });
            }
        })
    }
}

export const getters = {
    list: state => state.list,
  }
  